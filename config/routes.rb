Rails.application.routes.draw do
  # routes from chapter 5.3.2 - named routes
  root 'static_pages#home'
  get  '/help',    to: 'static_pages#help', as: 'helf' # named route
  get  '/about',   to: 'static_pages#about'
  get  '/contact', to: 'static_pages#contact'

  # below named routes, normally would be get/users/ and post/users/
  get '/signup', to: 'users#new' # no need to add "_path" in ''!!!
  post '/signup', to: 'users#create'

  # sessions routes
  get '/login', to: 'sessions#new'
  post '/login', to: 'sessions#create'
  delete '/logout', to: 'sessions#destroy'

  # resources for account activation chapter 11
  resources :account_activations, only: [:edit]
  # chapter 12 password_resets
  resources :password_resets,     only: %i[new create edit update]
  resources :users
  resources :microposts, only: %i[create destroy]

  resources :users do
    member do
      get :following, :followers
    end
  end

  resources :relationships, only: %i[create destroy]
end
