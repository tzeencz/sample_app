class Relationship < ApplicationRecord
  belongs_to :follower, class_name: 'User'
  belongs_to :followed, class_name: 'User'
  # validations below can be commented in Rails5
  validates :follower_id, presence: true
  validates :followed_id, presence: true
end
