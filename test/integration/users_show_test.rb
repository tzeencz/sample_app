require 'test_helper'

class UsersShowTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:michael)
    @non_activated_user = users(:malory)
  end

  test 'show only activated users' do
    get user_path(@user)
    assert_template 'users/show'
    get user_path(@non_activated_user)
    assert_redirected_to root_url
  end
end
