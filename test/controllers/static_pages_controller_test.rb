require 'test_helper'

class StaticPagesControllerTest < ActionDispatch::IntegrationTest

  def setup
    @base_title = 'Ruby on Rails Tutorial Sample App'
  end

  test "should get root" do
    # get root_url - basic routes test
    get root_path
    assert_response :success
    assert_select 'title', "#{@base_title}"
  end

  # whole test connected with basic routes, not needed in named routes
  # test "should get home" do
  #   get static_pages_home_url
  #   assert_response :success
  #   assert_select 'title', "#{@base_title}"
  # end

  test "should get help" do
    # get static_pages_help_url  - basic routes test
    get helf_path
    assert_response :success
    assert_select 'title', "Help | #{@base_title}"
  end

  test "should get about" do
    # get static_pages_about_url - basic routes test
    get about_path
    assert_response :success
    assert_select 'title', "About | #{@base_title}"
  end

  test "should get contact" do
    # get static_pages_contact_url - basic routes test
    get contact_path
    assert_response :success
    assert_select 'title', "Contact | #{@base_title}"
  end

end
